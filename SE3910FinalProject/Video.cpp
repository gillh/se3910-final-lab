/*
* Video.cpp
* Created on: May 21, 2016
* Authors:  Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
* This is an implementation of the Beaglebone Video interface, which describes
* a device that can record video and send it to the remote server.
*/

#include "Video.h"

/*
 * Constructs a new Video device
 *
 * @param host The hostname of the server, typically its ip address
 * @param port The port on which the server is listening for video
 * @param videoPort The port of the video device
 * @param width The width of the frames to capture
 * @param height The height of the frames to capture
*/
Video::Video(char *host, uint16_t port, int videoPort, int width, int height) {
    struct sockaddr_in serv_addr;
    struct hostent *server;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        cerr << "ERROR, couldn't initialize socket" << endl;
        exit(-1);
    }
    server = gethostbyname(host);
    if (server == NULL) {
        cerr << "ERROR, no such host " << host << endl;
        exit(-1);
    }
    memset((void *) &serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    memcpy((void *) &serv_addr.sin_addr.s_addr, (void *) server->h_addr, (size_t) server->h_length);
    serv_addr.sin_port = htons(port);
    int hresult = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
    if (hresult != 0) {
        cerr << "could not connect on the socket, hresult: " << hresult << endl;
        exit(-1);
    }
    videoCapture = new VideoCapture(videoPort);
    videoCapture->open(videoPort);
    videoCapture->set(CV_CAP_PROP_FRAME_WIDTH, width);
    videoCapture->set(CV_CAP_PROP_FRAME_HEIGHT, height);
    if (!videoCapture->isOpened()) {
        cerr << "Failed to connect to the camera." << endl;
        exit(-1);
    }
}

/**
 * Retrieves a frame from video device.
 */
void Video::capture() {
    videoCapture->grab();
    videoCapture->retrieve(frame, 0);
}

/**
 * Sends a video frame with an accompanying header describing the
 * 	- size of the frame data
 * 	- width of the frame
 * 	- height of th frame
 * to the server
 */
void Video::sendVideo() {
    Mat frameCopy(frame);
    cvtColor(frameCopy, frameCopy, CV_BGR2RGB, 3);
    int imageSize = frameCopy.total() * frameCopy.elemSize();
    vector<int> image_header;
    image_header.push_back(htonl(imageSize));
    image_header.push_back(htonl(frameCopy.rows));
    image_header.push_back(htonl(frameCopy.cols));

    int headerBytesSent = send(sockfd, image_header.data(), sizeof(image_header), 0);
    if (headerBytesSent < 0) {
        cerr << "Error Sending Header" << endl;
    }

    int frameBytesSent = send(sockfd, frameCopy.data, imageSize, 0);;
    if (frameBytesSent < 0) {
        cerr << "Error Sending Bytes" << endl;
    }
}

/**
 * Starts capturing frames from video device on a seperate thread
 */
void Video::start() {
    threadRunning = true;
    thread = std::thread(&Video::run, this);
}

/**
 * Continously captures and sends frames to the server 
 */
void Video::run() {
    while (threadRunning) {
        capture();
        sendVideo();
    }
}

/**
 * Stops the capturing and sending of frames to the server
 */
void Video::shutdown() {
    threadRunning = false;
    thread.join();
}

/**
 * Destructor
 */
Video::~Video() {
    delete videoCapture;
}
