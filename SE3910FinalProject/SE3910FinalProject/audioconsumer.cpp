/*
 * audioconsumer.cpp
 *
 * Created on: May 21, 2016
 * Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 * This represents the audio server that will recieve audio from the client and play it
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include "audioconsumer.h"
#include <thread>

/*
 * Constructor
 * Stores the port number and the device name
 * @param portNumber the port number to listen on
 * @param deviceName the name of the device
 */
AudioConsumer::AudioConsumer(int portNumber, char* deviceName){
    this->portNumber = portNumber;
    this->deviceName = deviceName;
}

/*
 * Function to setup and run the server to begin listening for data and playing back audio
 */
void AudioConsumer::runServer(){
    int sockfd, newsockfd, portno;
    unsigned int clilen;
    struct sockaddr_in serv_addr, cli_addr;

    // Initialize audio playback device
    AudioInterface *playback;
    playback = new AudioInterface(deviceName, SAMPLING_RATE, NUMBER_OF_CHANNELS, SND_PCM_STREAM_PLAYBACK);
    playback->open();

    // Allocate large buffer
    int bufferSize = playback->getRequiredBufferSize() * 4;
    char* buffer = (char*)malloc(bufferSize);

    // Create a socket.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    // If the return is less than 0, then the socket failed to create.
    if (sockfd < 0)
    {
      qDebug("ERROR opening socket");
    }

    // Initialize the buffer to all zeros.
    memset((void*) &serv_addr, 0, sizeof(serv_addr));

    // Obtain the port number as an integer.
    portno = portNumber;

    // Setup the server address structure.
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    // Bind the socket appropriately.
    if (bind(sockfd, (struct sockaddr *) &serv_addr,   sizeof(serv_addr)) < 0)
    {
      qDebug("ERROR on binding");
    }

    // Listen on the socket for an incoming connection.  The parameter is the number of connections that can be waiting / queued up.  5 is the maximum allowed by most systems.
    listen(sockfd,5);
    clilen = sizeof(cli_addr);

    // Block until a client has connected to the server.  This returns a file descriptor for the connection.
    newsockfd = accept(sockfd,
        (struct sockaddr *) &cli_addr,
        &clilen);

    // If the return is less than 0l, there is an error.
    if (newsockfd < 0)
    {
      qDebug("ERROR on accept");
    }

    // Fill the buffer with all zeros.
    memset(buffer, 0, bufferSize);

    // Keep the playback device busy with a few frames of silence
    playback->write(buffer, bufferSize);
    playback->write(buffer, bufferSize);
    playback->write(buffer, bufferSize);

    int bytesRead = -1;
    while (bytesRead != 0) {
        // Fill the buffer with all zeros.
        memset(buffer, 0, bufferSize);

        // Read from the buffer when data arrives.  The max that can be read is 255.
        bytesRead = read(newsockfd, buffer, bufferSize);

        if (bytesRead < 0) {
          qDebug("ERROR reading from socket");
        } else {
          playback->write(buffer, bufferSize);
        }
    }

    playback->close();
    free(buffer);
    delete playback;

}

