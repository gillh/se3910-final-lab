/*
 * mainwindow.cpp
 *
 * Created on May 21, 2016
 * Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 *
 * This represent the main ui that will display the video being captured from remote client
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

/*
 * Constructor
 * Creates the user interface setup
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // Create a widget to hold a layout, which will hold all the UI widgets
    QWidget * wdg = new QWidget(this);
    QVBoxLayout *vlay = new QVBoxLayout(wdg);

    imageLabel = new QLabel();
    imageLabel-> setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    vlay->addWidget(imageLabel);

    wdg->setLayout(vlay);
    setCentralWidget(wdg);

    setWindowTitle(tr("Hardip Ryan Austin Nick"));
    resize(1080,720);

    connect(this,SIGNAL(updateImageLabel(QImage)),this,SLOT(showFrame(QImage)));        // Signal/ Slot setup to listen for frame updates
}

/*
 * Slot used to update the video frame
 */
void MainWindow::showFrame(QImage frame){
    imageLabel->setPixmap(QPixmap::fromImage(frame));
}

/*
 * Function used to emit an update to user inteface with new video frame
 */
void MainWindow::emitFrame(QImage frame){
    emit(updateImageLabel(frame));
}

/*
 * Destructor
 * Delete user interface object
 */
MainWindow::~MainWindow()
{
    delete ui;
}
