/*
 * mainwindow.h
 *
 * Created on May 21, 2016
 * Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 *
 * This represent the main ui that will display the video being captured from remote client
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPrinter>

class QLabel;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*
     * Constructor
     * Creates the user interface setup
     */
    explicit MainWindow(QWidget *parent = 0);

    /*
     * Destructor
     * Delete user interface object
     */
    ~MainWindow();

    /*
     * Function used to emit an update to user inteface with new video frame
     */
    void emitFrame(QImage frame);

private:
    /*
     * Reference to main window user interface
     */
    Ui::MainWindow *ui;

    /*
     * Reference to label to display video frames upon
     */
    QLabel *imageLabel;

public slots:
    /*
     * Slot used to update the video frame
     */
    void showFrame(QImage frame);
signals:
    /*
     * Signal to update the image label
     */
    void updateImageLabel(QImage frame);
};

#endif // MAINWINDOW_H
