/*
 * VideoConsumer.cpp
 *
 * Created on May 21, 2016
 * Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 *
 * This represents the video server that will listen for frame data and updates the UI to show video frames
 */

#include "VideoConsumer.h"
#include <iostream>
#include "mainwindow.h"
#include <opencv2/opencv.hpp>
using namespace std;

/*
 * Constructor
 * Stores the main window and initalizes bytes remaining
 *
 * @param window the reference to the main window
 */
VideoConsumer::VideoConsumer(MainWindow *window)
{
    mainWindow = window;
    bytesRemaining == 0;
}

/*
 * Starts the QTcp server and begins listening for a client to connect
 */
void VideoConsumer::runServer(){
    server = new QTcpServer();

    connect(server, SIGNAL(newConnection()),
      this, SLOT(acceptConnection()));

    server->listen(QHostAddress::Any, 2015);
}

/*
 * Destructor
 * Closes the client and the server
 */
VideoConsumer::~VideoConsumer()
{
  client->close();
  server->close();
}

/*
 * Slot that accepts the connection, sets the client reference, and begins reading as soon as data is available
 */
void VideoConsumer::acceptConnection()
{
  client = server->nextPendingConnection();

  connect(client, SIGNAL(readyRead()),
    this, SLOT(startRead()));
}


/*
 * Helper function used to convert a byte array to an integer
 *
 * @param array byte array representing an integer
 */
quint32 VideoConsumer::ArrayToInt(QByteArray array){
    QDataStream stream(array);
    quint32 intValue = 0;
    stream >> intValue;
}

/*
 * Helper function used to convert a byte array into an image frame
 *
 * @param frameBytes byte array representing a Mat object
 */
QImage VideoConsumer::createFrame(QByteArray frameBytes){
    cv::Mat frame(frameWidth,frameHeight,16,frameBytes.data());                             // reconstruct Mat object
    QImage frameImage(frame.data, frame.cols, frame.rows,frame.step,QImage::Format_RGB888); // create an image from Mat object
    return frameImage;
}

/*
 * Slot used to read and interpert data bytes coming from client
 */
void VideoConsumer::startRead()
{
    if(bytesRemaining == 0){                                        // if a new frame is to be constructed
        QByteArray headerBytes = client->read(16);                      // read header from client
        bytesRemaining = ArrayToInt(headerBytes.mid(0,4));              // interpert size of frame data
        frameWidth = ArrayToInt(headerBytes.mid(4,4));                  // interpert width of frame
        frameHeight = ArrayToInt(headerBytes.mid(8,4));                 // interpert height of frame
    }

    if(bytesRemaining >0){                                          // if all bytes to construct frame have not been read
        QByteArray frameData = client->read(bytesRemaining);            // read as much data from the client necessary to construct image
        bytesRemaining -= frameData.size();                             // decrease amount needed to read by how much has been read
        frameBytes.append(frameData);                                   // append to global frame data container
        if(bytesRemaining == 0){                                        // if all bytes to construct image have been read
            mainWindow->emitFrame(createFrame(frameBytes));                 // emit ui to update with new image frame
            frameBytes.clear();                                             // clear the global frame data container
        }
    }
}

