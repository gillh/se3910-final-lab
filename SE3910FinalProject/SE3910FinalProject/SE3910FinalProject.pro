#-------------------------------------------------
#
# Project created by QtCreator 2016-05-17T12:41:49
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SE3910FinalProject
TEMPLATE = app
INCLUDEPATH += /usr/include/opencv
LIBS += -L/user/lib -lopencv_core -lasound
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    VideoConsumer.cpp \
    ../AudioServer/AudioInterface.cpp \
    audioconsumer.cpp

HEADERS  += \
    mainwindow.h \
    ui_mainwindow.h \
    VideoConsumer.h \
    ../AudioServer/AudioInterface.h \
    audioconsumer.h

FORMS    += mainwindow.ui \
    ../mainwindow.ui
