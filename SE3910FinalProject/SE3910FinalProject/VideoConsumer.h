#ifndef VIDEOCONSUMER_H
#define VIDEOCONSUMER_H


#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <thread>
#include "mainwindow.h"

class VideoConsumer: public QObject
{
Q_OBJECT
private:
    MainWindow *mainWindow;
public:
    /*
     * Constructor
     * Stores the main window and initalizes bytes remaining
     *
     * @param window the reference to the main window
     */
      VideoConsumer(MainWindow *window);

      /*
       * Destructor
       * Closes the client and the server
       */
      ~VideoConsumer();

      /*
       * Helper function used to convert a byte array to an integer
       *
       * @param array byte array representing an integer
       */
      quint32 ArrayToInt(QByteArray array);

      /*
       * Helper function used to convert a byte array into an image frame
       *
       * @param frameBytes byte array representing a Mat object
       */
      QImage createFrame(QByteArray frameBytes);

      /*
       * Starts the QTcp server and begins listening for a client to connect
       */
      void runServer();


public slots:
      /*
       * Slot that accepts the connection, sets the client reference, and begins reading as soon as data is available
       */
      void acceptConnection();

      /*
       * Slot used to read and interpert data bytes coming from client
       */
      void startRead();
private:
      /*
       * Global QTcpServer object reference
       */
      QTcpServer* server;

      /*
       * Global QTcpSocket object reference
       */
      QTcpSocket* client;

      /*
       * Global frame image data bytes container
       */
      QByteArray frameBytes;

      /*
       * Number of bytes needed to create next frame image
       */
      quint32 bytesRemaining;

      /*
       * Height of frame
       */
      quint32 frameHeight;

      /*
       * Width of frame
       */
      quint32 frameWidth;
};

#endif // VIDEOCONSUMER_H
