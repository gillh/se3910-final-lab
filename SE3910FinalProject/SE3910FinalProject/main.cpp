/*
 * main.cpp
 *
 * Created on: May 21, 2016
 * Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 * Main class that will run the audio and video server on seperate threads
 */

#include "mainwindow.h"
#include <QApplication>
#include <QtCore>
#include <QtNetwork>
#include "VideoConsumer.h"
#include "audioconsumer.h"
#include <QThread>
#include <QObject>
#include <thread>

/*
 * Static function to start the video server
 * @param object
 */
static void* staticPlayVideo(void* videoConsumer){
    qDebug()<< "video";
    static_cast<VideoConsumer*>(videoConsumer)->runServer();
    return NULL;
}

/*
 * Static function to start the audio server
 */
static void* staticPlayAudio(void* audioConsumer){
    qDebug()<< "audio";
    static_cast<AudioConsumer*>(audioConsumer)->runServer();
    return NULL;
}

AudioConsumer *audioConsumer;
VideoConsumer *videoConsumer;
std::thread *audioThread;
std::thread *videoThread;


/*
 * Main method that will start audio and video server on seperate threads
 * @param argc number of arguments
 * @param argv array of arguments
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow *w = new MainWindow();
    w->show();

    videoConsumer = new VideoConsumer(w);
    audioConsumer = new AudioConsumer(2017,"plughw:0");

    audioThread = new std::thread(staticPlayAudio, (void *) audioConsumer);
    videoThread = new std::thread(staticPlayVideo, (void *) videoConsumer);

    audioThread->join();
    videoThread->join();

    return a.exec();
}
