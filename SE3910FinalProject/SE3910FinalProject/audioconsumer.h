/*
 * audioconsumer.h
 *
 * Created on: May 21, 2016
 * Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 * This represents the audio server that will recieve audio from the client and play it
 */

#ifndef AUDIOCONSUMER_H
#define AUDIOCONSUMER_H

#include <QObject>
#include "../../SE3910FinalProject/AudioServer/AudioInterface.h"

class AudioConsumer: public QObject
{
    Q_OBJECT
private:
    /*
     * Port number to listen on
     */
    int portNumber;

    /*
     * Name of device
     */
    char* deviceName;
public:
    /*
     * Constructor
     * Stores the port number and the device name
     * @param portNumber the port number to listen on
     * @param deviceName the name of the device
     */
    AudioConsumer(int portNumber, char* deviceName);

    /*
     * Destructor
     */
    ~AudioConsumer();

    /*
     * Function to setup and run the server to begin listening for data and playing back audio
     */
    void runServer();
public slots:

};
#endif // AUDIOCONSUMER_H
