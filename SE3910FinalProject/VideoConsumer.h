#ifndef VIDEOCONSUMER_H
#define VIDEOCONSUMER_H


#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include "mainwindow.h"

class VideoConsumer: public QObject
{
Q_OBJECT
private:
    MainWindow *mainWindow;
public:
  VideoConsumer(QObject * parent = 0, MainWindow mainWindow);
  ~VideoConsumer();
public slots:
  void acceptConnection();
  void startRead();
private:
  QTcpServer server;
  QTcpSocket* client;
};

#endif // VIDEOCONSUMER_H
