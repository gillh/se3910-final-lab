#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // Create a widget to hold a layout, which will hold all the UI widgets
    QWidget * wdg = new QWidget(this);
    QVBoxLayout *vlay = new QVBoxLayout(wdg);

    imageLabel = new QLabel();
    imageLabel-> setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    vlay->addWidget(imageLabel);

    wdg->setLayout(vlay);
    setCentralWidget(wdg);

    setWindowTitle(tr("Hardip Ryan Austin Nick"));
    resize(1080,720);
}

void MainWindow::showFrame(QByteArray frameData){
    QPixmap p;
    p.loadFromData(QByteArray::fromBase64(frameData), "PNG");
    imageLabel->setPixmap(p);
}

MainWindow::~MainWindow()
{
    delete ui;
}
