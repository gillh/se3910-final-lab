#ifndef VIDEO_H
#define VIDEO_H

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <netdb.h>
#include <thread>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class Video {
private:
    int sockfd;
    VideoCapture *videoCapture;
    std::thread thread;
    bool threadRunning;
    Mat frame;

public:
    //Video(const Video&);

    Video(char *host, uint16_t port, int videoPort, int width, int height);

    void capture();

    void sendVideo();

    void start();

    void run();

    void shutdown();

    ~Video();
};

#endif