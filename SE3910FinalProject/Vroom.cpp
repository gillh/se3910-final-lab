/*
 * Vroom.cpp
 *
 *  Created on: May 20, 2016
 *      Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 *      This is the driver for the Beaglebone AV conferencing client.
 */

#include "Audio.h"
#include "Video.h"

using namespace std;

/**
 * Captures hardware and network parameters from the command line arguments,
 * starts the audio and video threads, and joins on them, allowing them to work
 * to completion.
 */
int main(int argc, char *argv[]) {

    char *audioHW;
    char *hostname;
    uint16_t audioPort;
    uint16_t videoPort;
    int seconds;

    if (argc != 6) {
        cout << "Usage: <avconferenceclient> <audio capture hardware device> <destination server machine> " <<
                "<destination audio port> <destination video port> <length of time to send in seconds>" <<
                endl;
        exit(1);
    }

    audioHW = argv[1];
    hostname = argv[2];
    audioPort = (uint16_t) atoi(argv[3]);
    videoPort = (uint16_t) atoi(argv[4]);
    seconds = atoi(argv[5]);

    Audio *a = new Audio(strdup(audioHW), audioPort, hostname, seconds);
    Video *v = new Video(hostname, videoPort, 0, 320, 240);

    a->start();
    a->stop();

    usleep(10000000);

    v->start();
    v->shutdown();

    delete a;
    delete v;

    cout << "ending" << endl;
}
