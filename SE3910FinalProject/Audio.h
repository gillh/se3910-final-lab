/*
 * Audio.h
 *
 *  Created on: May 20, 2016
 *      Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 *      This is the Beaglebone Audio interface, defining what a class needs to record, measure, and send audio data to the remote server.
 */

#include "AudioInterface.h"

#include <thread>
#include <iostream>
#include <sys/socket.h>
#include <mutex>
#include <unistd.h>

using namespace std;

class Audio {
private:
    /**
     * The file descriptor for the socket used to send audio to the server.
     */
    int sockfd;
    
    /**
     * The thread on which audio data is captured and sent to the server.
     */
    std::thread captureThread;
    
    /**
     * The thread on which the volume is measured.
     */
    std::thread measureThread;
    
    /**
     * The buffer for holding audio data to send to the server.
     */
    char *buffer;
    
    /**
     * The buffer for holding audio data whose volume is measured.
     */
    char *measureBuffer;
    
    /**
     * The device driver interface for an audio recording device.
     */
    AudioInterface *ai;
    
    /**
     * The size of the audio capturing buffer.
     */
    int bufferSize;
    
    /**
     * The number of bytes of audio data to capture in order to record
     * secondsToRun seconds worth of audio.
     */
    int bytesToCapture;
    
    /**
     * The number of bytes of audio data captured into measureBuffer that
     * will go into the next volume report.
     */
    int bytesToReportOn;
    
    /**
     * The number of seconds of audio to record, send to the server, and measure.
     */
    int secondsToRun;

public:
    /**
     * Constructs a new Audio device given the hardware name of the soundcard, the
     * port on which the server will receieve the audio, the server's hostname,
     * and how many seconds of audio to capture. This method will instantiate the 
     * socket connection and the buffers used to store audio data.
     *
     * @param deviceName The name of soundcard (ex: plughw:1)
     * @param port The port on which the server is listening for audio
     * @param host The hostname of the server, typically its ip address
     * @param seconds The number of seconds of audio to capture
     */
    Audio(char *deviceName, uint16_t port, char *host, int seconds);

    /**
     * Fills the audio capture buffer from audio data straight from the
     * audio device. Additionally, copies the data it reads into the next
     * available position in the measurement buffer.
     *
     * @return The number of bytes read from the audio device.
     */
    int capture();

    /**
     * Sends the audio data currently in the audio buffer across the socket
     * to the server.
     *
     * @return The number of bytes sent to the socket.
     */
    int send();

    /**
     * Measures the volume every second of the audio captured in the last second.
     * This method runs on its own thread and sleeps for a second. Every time it wakes up,
     * it takes the data currently in the measurement buffer and determines the average
     * volume of the data in the buffer. This will happen once per second up to the number
     * of seconds of audio recorded. 
     */
    void measure();

    /**
     * Starts the capture thread and the measurement thread.
     * The thread priority of the capture thread is set to be high,
     * while the measurement thread's priority is set much lower.
     */
    void start();   
    
    /**
     * This method runs on the audio capture thread. This method will continue to
     * capture audio from the recording device and sending it across the network until
     * it has captured and sent secondsToRun worth of data.
     */
    void run();

    /**
     * Stops this audio device, joining on the capture and measurement threads.
     */
    void stop();

    /**
     * Destroys the audio device, closing and deleting the underlying audio interface,
     * and frees the capture and measurement buffers.
     */
    ~Audio();
};
