cmake_minimum_required(VERSION 3.5)
project(rts)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -lpthread -lEBBLibrary")

set(SOURCE_FILES Vroom.cpp AudioInterface.h AudioInterface.cpp Audio.h Audio.cpp)
add_executable(rts ${SOURCE_FILES})