#include "VideoConsumer.h"
#include <iostream>
#include "mainwindow.h"
using namespace std;

VideoConsumer::VideoConsumer(QObject* parent, MainWindow mainWindow): QObject(parent)
{
  this->mainWindow = mainWindow;

  connect(&server, SIGNAL(newConnection()),
    this, SLOT(acceptConnection()));

  server.listen(QHostAddress::Any, 2017);
}

VideoConsumer::~VideoConsumer()
{
  server.close();
}

void VideoConsumer::acceptConnection()
{
  client = server.nextPendingConnection();

  connect(client, SIGNAL(readyRead()),
    this, SLOT(startRead()));
}

void VideoConsumer::startRead()
{
  char buffer[400000] = {0};
  qDebug() << "df" << buffer;
  client->read(buffer, client->bytesAvailable());
  qDebug() << "Got Something";
  this->mainWindow->showFrame(QByteArray(buffer));
  //client->close();
}
