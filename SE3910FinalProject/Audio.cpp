/*
 * Audio.cpp
 *
 *  Created on: May 20, 2016
 *      Author: Ryan Forward, Hardip Gill, Austin Hartline, Nick Boddy
 *      This is the implementation of the Beaglebone Audio interface, which describes a device that can record audio
 *      and send it to the remote server.
 */

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <pthread.h>
#include "Audio.h"
#include <unistd.h>
#include <math.h>

/**
 * Constructs a new Audio device given the hardware name of the soundcard, the
 * port on which the server will receieve the audio, the server's hostname,
 * and how many seconds of audio to capture. This method will instantiate the 
 * socket connection and the buffers used to store audio data.
 *
 * @param deviceName The name of soundcard (ex: plughw:1)
 * @param port The port on which the server is listening for audio
 * @param host The hostname of the server, typically its ip address
 * @param seconds The number of seconds of audio to capture
 */
Audio::Audio(char *deviceName, uint16_t port, char *host, int seconds) {
    struct sockaddr_in serv_addr;
    struct hostent *server;

    // Prepare audio device
    ai = new AudioInterface(deviceName, SAMPLING_RATE, NUMBER_OF_CHANNELS, SND_PCM_STREAM_CAPTURE);
    ai->open();

    // Connect to conferencing server
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        cerr << "ERROR, couldn't initialize socket" << endl;
        exit(-1);
    }
    // Get the hostname.
    server = gethostbyname(host);
    // If the server is NULL, there is no such host.
    if (server == NULL) {
        cerr << "ERROR, no such host " << host << endl;
        exit(-1);
    }
    // Set the values in the buffer to zero.
    memset((void *) &serv_addr, 0, sizeof(serv_addr));
    // Set up the server address type.
    serv_addr.sin_family = AF_INET;
    memcpy((void *) &serv_addr.sin_addr.s_addr, (void *) server->h_addr, (size_t) server->h_length);
    serv_addr.sin_port = htons(port);
    // connect to the socket.
    int hresult = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
    if (hresult != 0) {
        cerr << "could not connect on the socket, hresult: " << hresult << endl;
        exit(-1);
    }

    // Determine how many bytes need to be captured.
    secondsToRun = seconds;
    bytesToCapture = SAMPLING_RATE * secondsToRun * NUMBER_OF_CHANNELS * BYTES_PER_SAMPLE;  
   
    // Initialize capture buffer
    bufferSize = ai->getRequiredBufferSize();    
    buffer = (char *) malloc((size_t) bufferSize);    
    memset(buffer, 0, bufferSize);
    
    // Allocate buffer for measured data
    measureBuffer = (char *) malloc((size_t) (bytesToCapture/secondsToRun) * 2);
    memset(measureBuffer, 0, (bytesToCapture/secondsToRun) * 2);
}

/**
 * Destroys the audio device, closing and deleting the underlying audio interface,
 * and frees the capture and measurement buffers.
 */
Audio::~Audio() {
    ai->close();
    delete ai;
    free(buffer);
    free(measureBuffer);
}

/**
 * Fills the audio capture buffer from audio data straight from the
 * audio device. Additionally, copies the data it reads into the next
 * available position in the measurement buffer.
 *
 * @return The number of bytes read from the audio device.
 */
int Audio::capture() {

    int bytesRead;
 
    bytesRead = ai->read(buffer);
    
    if (bytesRead < 0) {
        cerr << "in Audio::capture() --- error code " << bytesRead << endl;
    }
    
    memcpy(&measureBuffer[bytesToReportOn], buffer, bytesRead * sizeof(char));
    bytesToReportOn += bytesRead;
    
    return bytesRead;
}

/**
 * Sends the audio data currently in the audio buffer across the socket
 * to the server.
 *
 * @return The number of bytes sent to the socket.
 */
int Audio::send() {
    int bytesSent = 0;
    bytesSent = write(sockfd, buffer, bufferSize);

    // iF N IS LESS THAN 0, AN ERROR HAS OCCURRED WRITING TO THE SOCKET.
    if (bytesSent < 0) {
        cerr << "ERROR writing to socket" << endl;
    }
    
    return bytesSent;
}

/**
 * Measures the volume every second of the audio captured in the last second.
 * This method runs on its own thread and sleeps for a second. Every time it wakes up,
 * it takes the data currently in the measurement buffer and determines the average
 * volume of the data in the buffer. This will happen once per second up to the number
 * of seconds of audio recorded. 
 */
void Audio::measure() {
    int timesRun = 0;
    while (timesRun < secondsToRun) {
        
        // Prepare for next report
        double oneSecAverage = 0.0;
        usleep(1000000);
        
        for(int x = 0; x < bytesToReportOn; x++) {
            oneSecAverage += pow(((double)measureBuffer[x])/32768.0, 2);
        }
        
        oneSecAverage /= bytesToReportOn;
        
        cout << "dB for the last second: " << 20 * log10(oneSecAverage) << endl;
        
        // Clean up for next report
        memset(measureBuffer, 0, (bytesToCapture/secondsToRun) * 2);
        bytesToReportOn = 0;
        timesRun += 1;
    }
    
}

/**
 * Starts the capture thread and the measurement thread.
 * The thread priority of the capture thread is set to be high,
 * while the measurement thread's priority is set much lower.
 */
void Audio::start() {
    // Start audio capture thread
    captureThread = std::thread(&Audio::run, this);  
    // Set thread's priority to be the highest using the underlying p_thread
    sched_param sch_params;
    sch_params.sched_priority = 1;
    if(pthread_setschedparam(captureThread.native_handle(), SCHED_RR, &sch_params)) {
        std::cerr << "Failed to set capture Thread scheduling : " << strerror(errno) << std::endl;
    }
    
    // Also, start audio measurement thread
    measureThread = std::thread(&Audio::measure, this);
    bytesToReportOn = 0;
    
    // Set thread's priority to be the highest using the underlying p_thread
    sched_param measure_sch_params;
    measure_sch_params.sched_priority = 5;
    if(pthread_setschedparam(captureThread.native_handle(), SCHED_RR, &measure_sch_params)) {
        std::cerr << "Failed to set measure Thread scheduling : " << strerror(errno) << std::endl;
    }
}

/**
 * This method runs on the audio capture thread. This method will continue to
 * capture audio from the recording device and sending it across the network until
 * it has captured and sent secondsToRun worth of data.
 */
void Audio::run() {
    int playedSoFar = 0;
    while (playedSoFar < bytesToCapture) {
        capture();
        send();
        playedSoFar += bufferSize;
    }
}

/**
 * Stops this audio device, joining on the capture and measurement threads.
 */
void Audio::stop() {
    captureThread.join();
    measureThread.join();
}

